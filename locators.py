from selenium.webdriver.common.by import By


# _TF - text field
# _IV  - parameter input values
# _CB - check box
# _LB - label
# _BT - buttonProductPage
# _DD - dropdown
# _RB - radio button
# for maintainability we can seperate web objects by page name


class LoginPageLocators(object):
    USERNAME_TF = (By.XPATH, "//input[@id='username']")
    PASSWORD_TF = (By.XPATH, "//input[@id='password']")
    SUBMIT_BT = (By.XPATH, "//button[@class='login-page-form-button']")
    REMEMBER_ME_CB = (By.XPATH, "//input[@id='remember-me']")
    ALERT_TM = (By.XPATH, "//div[@class='alert alert-danger fade in fade-in-alert']")


class LandingPage(object):
    LOGO = (By.XPATH, "//a[@class='beatport-logo']")
    COOKIES_AGREE = (By.XPATH, '/html/body/div[3]/div/div/button')

    class NavigationBar:
        ACCOUNT_SETTINGS_EL = (By.XPATH, "//div[@id='head-account-icon']")

        ACCUT_SETTING_LINK = (By.XPATH, "//*[@href='/account/profile']")
        ACCUT_SETTING_LINK2 = (By.XPATH, "//a[contains(text(),'Account Settings')]")
        MY_LIBARY_EL = (By.XPATH, "//div[@class='tooltip__container']")
        MY_FALLOWINGS_EL = (By.XPATH, "//div[@id='head-mybeatport-icon']")
        MY_USER_NAME_LB = (By.XPATH, "//span[@class='head-account-content']")
        SEARCH_BAR = (By.XPATH, "//input[@class='text-input__input text-input__input--no-margin']")


class SearchResultPage(object):
    NEXT_PAGE_BT = (By.XPATH, "//div[@class='pagination-container pagination-top-container']//a[@class='pag-next']")
    PAGES_SWITCHER = (
    By.XPATH, "//div[@class='pagination-container pagination-top-container']//div[@class='pag-numbers']")
    TRACKS_BT = (By.XPATH, "//a[@class='page-selector-link'][contains(text(),'Tracks')]")
    ALL_TRACK_ME = (By.XPATH, "//ul[contains(@class,'ec-bucket')]/li[@class='bucket-item ec-item track']")


class AccountSettingsPage(object):
    class Profile:
        FILE_UPLOAD_IN = (By.XPATH, "//input[@type='file']")
        BROWSER_BT = (By.XPATH, '//*[@id="image-uploader__browse-button"]')
        SAVE_AVATAR_BT = (By.XPATH, "//button[@id='image-uploader__save-button']")
        NAME_TF = (By.XPATH, "//input[@id='dj_name']")
        NAME_ERROR_MESSAGE_LB = (
        By.XPATH, "/html[1]/body[1]/div[2]/div[1]/section[1]/main[1]/div[2]/form[1]/ul[1]/li[2]/p[1]")
        DJ_URL_TF = (By.XPATH, "//input[@id='dj_slug']")
        DJ_URL_ERROR_MESSAGE1_LB = (
        By.XPATH, "/html[1]/body[1]/div[2]/div[1]/section[1]/main[1]/div[2]/form[1]/ul[1]/li[3]/p[1]")
        DJ_URL_ERROR_MESSAGE2_LB = (
        By.XPATH, "/html[1]/body[1]/div[2]/div[1]/section[1]/main[1]/div[2]/form[1]/ul[1]/li[3]/p[2]")
        DESCRIPTION_TA = (By.XPATH, "//textarea[@id='dj_bio']")
        LOCATION_TF = (By.XPATH, "//input[@id='autocomplete']")
        SOUND_CLOUD_URL_TF = (By.XPATH, "//input[@id='dj_soundcloud_id']")
        SAUND_CLOAD_URL_EROOR_MESAGE_LB = (
        By.XPATH, "/html[1]/body[1]/div[2]/div[1]/section[1]/main[1]/div[2]/form[1]/ul[1]/li[6]/p[1]")
        SUBMIT_CHANGES_BT = (By.XPATH, "//button[@name='save']")
        SUCCESS_MESSAGE_BAR = (By.XPATH, '//*[@id="pjax-inner-wrapper"]/section/main/p/span[2]')
        # DJ GENRES
        ALL_GENRES = (By.XPATH, "//div[@class='genre-select__genre']")
        GENRES_AFRO_HAUSE_LB = (By.XPATH, "//label[contains(text(),'Afro House')]")
        GENRES_BASS_HAUSE_LB = (By.XPATH, "//label[contains(text(),'Bass House')]")
        GENRES_HARD_TECHNO_LB = (By.XPATH, "//label[contains(text(),'Hard Techno')]")
        GENRES_HIP_HOP_RNB_LB = (By.XPATH, "//label[contains(text(),'Hip-Hop / R&B')]")