import Base_Page
import pytest
from locators import *
from data_driven import *
import time
import mysql.connector


@pytest.mark.usefixtures("setup")
class TestBrowser:

    # This TC verify if user is able to login with correct login and password
    def test_Sign_In_Positive(self):
        driver = Base_Page.LoginPage(self.driver)
        driver.fill_credentials('patryk_test', 'Test_test123')
        driver.ensure_titles_are_displayed("Beatport: DJ & Dance Music, Tracks & Mixes")
        driver.ensure_element_is_displayed(*LandingPage.NavigationBar.ACCOUNT_SETTINGS_EL)
        driver.ensure_element_is_displayed(*LandingPage.NavigationBar.MY_LIBARY_EL)
        driver.ensure_element_is_displayed(*LandingPage.NavigationBar.MY_FALLOWINGS_EL)
        driver.comparison_texts(driver.get_whole_text_from_elements(*LandingPage.NavigationBar.MY_USER_NAME_LB),"patryk_test")
        driver.click_element(*LandingPage.COOKIES_AGREE)

    # This TC verify if user is able to edit Account Settings with correct dataset
    def test_Account_Settings_Profile_Positive(self):
        driver = Base_Page.BasePage(self.driver)
        driver.select_from_mauseover_menu(*LandingPage.NavigationBar.ACCOUNT_SETTINGS_EL,*LandingPage.NavigationBar.ACCUT_SETTING_LINK)
        driver.upload_file('/47588.jpg')
        driver.fill_field(*AccountSettingsPage.Profile.NAME_TF, "Patryk")
        driver.fill_field(*AccountSettingsPage.Profile.DJ_URL_TF, "djpatryk")
        driver.fill_field(*AccountSettingsPage.Profile.DESCRIPTION_TA, "Hello World")
        driver.fill_field(*AccountSettingsPage.Profile.LOCATION_TF, "Wroclaw")
        driver.fill_field(*AccountSettingsPage.Profile.SOUND_CLOUD_URL_TF, "djpatryk")
        driver.click_multiple_elemenets(*AccountSettingsPage.Profile.ALL_GENRES)
        driver.click_element(*AccountSettingsPage.Profile.SUBMIT_CHANGES_BT)
        driver.ensure_element_is_displayed(*AccountSettingsPage.Profile.SUCCESS_MESSAGE_BAR)
        driver.comparison_texts(driver.get_whole_text_from_elements(*AccountSettingsPage.Profile.SUCCESS_MESSAGE_BAR),
                                "Your DJ Profile settings have been updated successfully!")

    # This TC verify if user is able to edit Account Settings with incorrect dataset
    def test_Account_Settings_Profile_Negative(self):
        driver = Base_Page.BasePage(self.driver)
        driver.click_element(*LandingPage.LOGO)

        driver.select_from_mauseover_menu(*LandingPage.NavigationBar.ACCOUNT_SETTINGS_EL,
                                          *LandingPage.NavigationBar.ACCUT_SETTING_LINK)
        driver.upload_file('/47588.jpg')
        driver.fill_field(*AccountSettingsPage.Profile.NAME_TF, "!")
        driver.fill_field(*AccountSettingsPage.Profile.DJ_URL_TF, "!")
        driver.fill_field(*AccountSettingsPage.Profile.DESCRIPTION_TA, "!")
        driver.fill_field(*AccountSettingsPage.Profile.LOCATION_TF, "!")
        driver.fill_field(*AccountSettingsPage.Profile.SOUND_CLOUD_URL_TF, "!")
        driver.click_element(*AccountSettingsPage.Profile.SUBMIT_CHANGES_BT)

        driver.comparison_texts(driver.get_whole_text_from_elements(*AccountSettingsPage.Profile.NAME_ERROR_MESSAGE_LB),
                                "DJ name must be between 3 and 45 characters long.")
        driver.comparison_texts(
            driver.get_whole_text_from_elements(*AccountSettingsPage.Profile.DJ_URL_ERROR_MESSAGE1_LB),
            "URL must be between 3 and 45 characters long.")
        driver.comparison_texts(
            driver.get_whole_text_from_elements(*AccountSettingsPage.Profile.DJ_URL_ERROR_MESSAGE2_LB),
            "URL must contain only letters, numbers, and dashes.")
        driver.comparison_texts(
            driver.get_whole_text_from_elements(*AccountSettingsPage.Profile.SAUND_CLOAD_URL_EROOR_MESAGE_LB),
            "SoundCloud URL must contain only letters, numbers, dashes, and underscores.")


    # Data Driven TC
    # This TC verify if user is able to use Search Bar
    # Additionally this TC verify if "Tracks" related to searched artist is equal to this given from dataset
    @pytest.mark.parametrize(
        artist_parameters,
        artist_DataDriven)
    def test_Search_Bar_By_Artist_Name(self, artistname,tracksamount):
        driver = Base_Page.SearchResultPage(self.driver)
        driver.SearchBarInput(artistname)
        driver.open_artist_profile(artistname)
        driver.click_element(*SearchResultPage.TRACKS_BT)
        c=driver.get_text_from_multiple_elemenets(*SearchResultPage.PAGES_SWITCHER)
        d=driver.count_all_visable_tracks(c,*SearchResultPage.ALL_TRACK_ME,*SearchResultPage.NEXT_PAGE_BT,tracksamount)
        driver.comparison_texts(d, tracksamount)
        pytest.tracks_amount_per_page.clear()


