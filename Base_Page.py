from locators import *
import time
import datetime
import os
import pytest
from datetime import date, timedelta
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import StaleElementReferenceException, TimeoutException, NoSuchElementException, \
    JavascriptException
import selenium.webdriver.support.ui as ui

from selenium.webdriver.common.keys import Keys


class BasePage():
    """Base class to initialize the base page that will be called from all pages"""

    def __init__(self, driver):

        self.driver = driver

    def wait_for_x_sec(self, amount):
        time.sleep(amount)



    def fill_field(self, types, field, text):
        element = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((types, field))
        )
        element.clear()
        element.send_keys(text)

    def fill_date(self, types, field, text):
        element = self.driver.find_element(types, field)
        self.driver.execute_script("arguments[0].value=arguments[1]", element, text)

    def find_text_on_the_page(self, list_of_text, text):
        for i in list_of_text:
            if text.find(i) != -1:
                assert True, 'True' + i
                print("true", i)
            else:
                assert False, 'False' + i

    def ensure_element_is_displayed(self, types, element):
        if WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((types, element))
        ):
            print('Element with XPATH: ', element, ' is displayed.')
        else:
            print('Element with XPATH: ', element, ' is not displayed.')

    def ensure_elements_are_displayed(self, types, element):
        if WebDriverWait(self.driver, 10).until(
                EC.presence_of_all_elements_located((types, element))
        ):
            print('Elements with XPATH: ', element, ' are displayed.')
        else:
            print('Elements with XPATH: ', element, 'is not displayed.')

    def select_from_mauseover_menu(self, types_menu, element_menu, types, element):
        try:
            self.wait_for_x_sec(2)
            firstLevelMenu = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((types_menu, element_menu)) and EC.presence_of_element_located(
                    (types, element))
            )
            actionchains = ActionChains(self.driver)
            actionchains.move_to_element(firstLevelMenu).perform()
            secondLevelMenu = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((types, element)) and EC.presence_of_element_located((types, element))
            )
            actionchains.move_to_element(secondLevelMenu).click(secondLevelMenu).perform()
        except JavascriptException:
            self.wait_for_x_sec(2)
            firstLevelMenu = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((types_menu, element_menu)) and EC.presence_of_element_located(
                    (types, element))
            )
            actionchains = ActionChains(self.driver)
            actionchains.move_to_element(firstLevelMenu).perform()
            secondLevelMenu = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((types, element)) and EC.presence_of_element_located((types, element))
            )
            actionchains.move_to_element(secondLevelMenu).click(secondLevelMenu).perform()

    def select_checkbox(self, types, element):
        element = self.driver.find_element(types, element)
        actionchains = ActionChains(self.driver)
        actionchains.click(element).perform()

    def alert_handling(self):
        try:
            WebDriverWait(self.driver, 3).until(EC.alert_is_present(),
                                                'Timed out waiting for PA creation ' +
                                                'confirmation popup to appear.')

            alert = self.driver.switch_to.alert
            alert.accept()
            print("Alert accepted")
            alert = self.driver.switch_to.default_content()
        except TimeoutException:
            print("No alert")

    def click_element(self, types, element):
        try:
            element = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((types, element)) and EC.presence_of_element_located((types, element))
            )
            self.driver.execute_script("arguments[0].click();", element)
        except StaleElementReferenceException as e:
            self.wait_for_x_sec(.300)
            self.driver.find_element(types, element).click()

    def click_multiple_elemenets(self, types, element):
        filters = self.driver.find_elements(types, element)
        for x in range(0, len(filters)):
            filters = self.driver.find_elements(types, element)
            actionchains = ActionChains(self.driver)
            actionchains.move_to_element(filters[x]).perform()
            actionchains.click(filters[x]).perform()
            self.driver.execute_script("window.scrollTo(0, window.scrollY + 50)")

    def upload_file(self, element):
        self.driver.find_element(*AccountSettingsPage.Profile.FILE_UPLOAD_IN).send_keys(os.getcwd() + element)
        self.click_element(*AccountSettingsPage.Profile.SAVE_AVATAR_BT)

    def ensure_titles_are_displayed(self, title):
        print('Title from test input: ', title, 'Title from page: ', self.driver.title)
        wait_for_title = WebDriverWait(self.driver, 10).until(lambda x: title in self.driver.title)

    def get_whole_text_from_elements(self, types, text_from):
        element = self.driver.find_elements(types, text_from)
        whole_text = element[0].text
        print(element[0].text)
        return whole_text

    def comparison_texts(self, element_from_page, element):
        if (str(element_from_page) in str(element)):
            print("True " + str(element_from_page) + " is in " + str(element))
            assert True, 'True'
        else:
            print("False " + str(element_from_page) + " is not in " + str(element))
            assert False, 'False'

    def get_text_from_multiple_elemenets(self, types, element):
        # matched_elements = self.driver.find_elements(types, element)
        try:
            if WebDriverWait(self.driver, 5).until(
                    EC.presence_of_all_elements_located((types, element))):
                matched_elements = self.driver.find_elements(types, element)
                texts = []
                for matched_element in matched_elements:
                    text = matched_element.text
                    texts.append(text)
                pages_amount = int(texts[-1][-1])
                print('Pages amount:', pages_amount)
                return pages_amount
        except TimeoutException:
            print('No Page Switcher at this page')
            pages_amount = 0
            return pages_amount


class SearchResultPage(BasePage):

    def SearchBarInput(self, text):
        # element = self.driver.find_element(*LandingPage.NavigationBar.SEARCH_BAR)
        element = WebDriverWait(self.driver, 30).until(
            EC.presence_of_element_located(LandingPage.NavigationBar.SEARCH_BAR)
        )
        element.click()
        self.driver.execute_script("arguments[0].value=arguments[1]", element, '')
        element.send_keys(text)
        self.wait_for_x_sec(4)
        element.send_keys(Keys.ARROW_DOWN)
        element.send_keys(Keys.ENTER)

    def open_artist_profile(self, artistname):
        element = WebDriverWait(self.driver, 30).until(
            EC.presence_of_element_located(
                (By.XPATH, "//p[@class='artist-name'][contains(text(),'{}')]".format(artistname)))
        )
        element.click()

    def count_all_visable_tracks(self, count, type_tracks, element_tracks, type_next, element_next, tracksamount):
        all_tracks = self.driver.find_elements(type_tracks, element_tracks)
        try:
            tab = []
            if WebDriverWait(self.driver, 5).until(EC.presence_of_all_elements_located((type_next, element_next))):
                element = self.driver.find_element(type_next, element_next)
                print('Tracks on 1 page: ', len(all_tracks))
                pytest.tracks_amount_per_page.append(len(all_tracks))
                amount = (sum(pytest.tracks_amount_per_page))
                print(amount)
                tab.append(amount)
                for x in range(1, count):
                    element.click()
                    self.wait_for_x_sec(2)
                    all_tracks = self.driver.find_elements(type_tracks, element_tracks)
                    print('Tracks on page: ', len(all_tracks))
                    pytest.tracks_amount_per_page.append(len(all_tracks))
                    amount = (sum(pytest.tracks_amount_per_page))
                    print(amount)
                    tab.append(amount)
        except TimeoutException:
            print('No Next Arrow')
            all_tracks = self.driver.find_elements(type_tracks, element_tracks)
            print('Tracks on page:', len(all_tracks))
            tab.append(len(all_tracks))
        except StaleElementReferenceException:
            element = self.driver.find_element(type_next, element_next)
            actionchains = ActionChains(self.driver)
            actionchains.click(element).perform()
            self.wait_for_x_sec(2)
            all_tracks = self.driver.find_elements(type_tracks, element_tracks)
            print('Tracks on page:', len(all_tracks))
            pytest.tracks_amount_per_page.append(len(all_tracks))
            amount = (sum(pytest.tracks_amount_per_page))
            print(amount)
            tab.append(amount)
        return (max(tab))


class LoginPage(BasePage):

    def fill_credentials(self, email, password):
        self.driver.find_element(*LoginPageLocators.USERNAME_TF).send_keys(email)
        self.driver.find_element(*LoginPageLocators.PASSWORD_TF).send_keys(password)
        self.driver.find_element(*LoginPageLocators.SUBMIT_BT).click()
