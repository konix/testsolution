import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

def pytest_configure():
    pytest.tracks_amount_per_page=[]


@pytest.fixture(scope="class")
def setup(request):
    print("initiating driver")
    options = Options()
    #options.add_argument("--headless")
    options.add_argument('--no-sandbox')
    options.add_argument("--disable-setuid-sandbox")
    options.add_argument('--ignore-certificate-errors')
    options.add_argument("--start-maximized")
    options.add_argument("--window-size=1920,1080")
    #driver = webdriver.Remote("http://127.0.0.1:4444/wd/hub", options=options)
    driver = webdriver.Chrome(executable_path="C:\\Selenium_Drivers\\chromedriver.exe", options=options)
    #driver = webdriver.Chrome(executable_path="/home/pakoni/Projects/TIA_automation/Selenium_drivers/chromedriver_linux64/chromedriver", options=options)


    driver.get("https://www.beatport.com/account/login")
    driver.implicitly_wait(5)

    request.cls.driver = driver
    yield driver
    driver.close()



