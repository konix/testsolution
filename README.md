### Prerequisite
According to your OS download and install Python. I recommend to use at least Python 3.6.10 version.
https://www.python.org/downloads/release/python-3610/ 

According to your OS and chrome version download the chromedriver execution file and save it in the scripts folder:
https://chromedriver.chromium.org/downloads
```             
C:\Selenium_Drivers
```
### Installation
* Install [pip](https://pip.pypa.io/en/stable/installing/) for package installation
* The recommended way to run your tests would be in [virtualenv](https://virtualenv.readthedocs.org/en/latest/). It will isolate the build from other setups you may have running and ensure that the tests run with the specified versions of the modules specified in the requirements.txt file.
	```$ pip install virtualenv```
* Create a virtual environment in your project folder the environment name is arbitrary.
	```$ virtualenv venv```
* Activate the environment:
	```$ source venv/bin/activate```
* Install the required packages:
	```$ pip install -r requirements.txt```
* Change path to your chrome driver in conifest.py file:
  WIN SAMPLE PATH:
  ```
  driver = webdriver.Chrome(executable_path="C:\\Selenium_Drivers\\chromedriver.exe", options=options)
  ```
  LINUX SAMPLE PATH:
  ``` 
  driver = webdriver.Chrome(executable_path="/home/pakoni/Projects/TIA_automation/Selenium_drivers/chromedriver_linux64/chromedriver", options=options)
  ```


### How to Run
In the terminal move to the project directory and enter:
```
$ py.test -s -v Test_Runner.py
```
### Tested with:
OS: Ubuntu 18.04/Win10
Chrome: 79.0.3945.79(64)
ChromeDriver: 79.0.3945.36/ chromedriver_linux64.zip



